CREATE DATABASE webapp;

USE webapp;

CREATE TABLE empresa (
	id int(5) auto_increment,
	company_name varchar(200),
	primary key(id)
)ENGINE=InnoDB;

CREATE TABLE funcionario (
	id int(5) auto_increment,
	id_empresa int(5),
	first_name varchar(20),
	last_name varchar(20),
	age int,
	country varchar(50),
	city	varchar(50),
	state	varchar(50),
	office	varchar(100),
	primary key (id),
	foreign key(id_empresa) references empresa(id)
)ENGINE=InnoDB;

INSERT INTO empresa VALUES ('1', 'Empresa1');
INSERT INTO empresa VALUES ('2', 'Empresa2');
INSERT INTO empresa VALUES ('3', 'Empresa3');

INSERT INTO funcionario VALUES ('1', '1', 'FirstName1', 'LastName1', '20', 'Brasil', 'Curitiba', 'Paraná', 'Developer');
INSERT INTO funcionario VALUES ('2', '1', 'FirstName2', 'LastName2', '30', 'Brasil', 'Maringa', 'Paraná', 'Desinger');
INSERT INTO funcionario VALUES ('3', '2', 'FirstName3', 'LastName3', '40', 'Brasil', 'Maringa', 'Paraná', 'Publicidade e propaganda');
INSERT INTO funcionario VALUES ('4', '2', 'FirstName4', 'LastName4', '45', 'Brasil', 'Maringa', 'Paraná', 'Auxiliar de informática');
INSERT INTO funcionario VALUES ('5', '2', 'FirstName5', 'LastName5', '25', 'Brasil', 'Maringa', 'Paraná', 'Secretária');
INSERT INTO funcionario VALUES ('6', '3', 'FirstName6', 'LastName6', '45', 'Brasil', 'Maringa', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('7', '3', 'FirstName7', 'LastName7', '23', 'Brasil', 'Pato Branco', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('8', '3', 'FirstName8', 'LastName8', '16', 'Brasil', 'Pato Branco', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('9', '3', 'FirstName9', 'LastName9', '18', 'Brasil', 'Pato Branco', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('10', '3', 'FirstName10', 'LastName10', '19', 'Brasil', 'Pato Branco', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('11', '3', 'FirstName11', 'LastName11', '20', 'Brasil', 'Curitiba', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('12', '3', 'FirstName12', 'LastName12', '22', 'Brasil', 'Curitiba', 'Paraná', 'Cobrador');
INSERT INTO funcionario VALUES ('13', '3', 'FirstName13', 'LastName13', '23', 'Brasil', 'Curitiba', 'Paraná', 'Cobrador');


